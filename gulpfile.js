var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;
elixir.config.autoprefix = true;
elixir.config.production = true;
//elixir.config.css.autoprefix.options = ['> 0%'];
elixir.config.css.autoprefix.options = ['last 2 versions'];
elixir.config.css.autoprefix.cascade = true;

elixir(function(mix) {
    mix
        .sass('app.scss')
        .browserify('app.js', 'public/js/app.js')
});
