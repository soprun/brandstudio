<div class="grid-center">
    <div class="col-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Register</div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="grid-center">
                        <div class="col-5">
                            <label class="control-label">Name</label>
                        </div>
                        <div class="col-7">
                            <input type="text" class="form-control" ng-model="user.name">
                            <p class="text-danger" ng-repeat="error in errors.name">@{{ error }}</p>
                        </div>
                        <div class="col-5">
                            <label class="control-label">E-Mail Address</label>
                        </div>
                        <div class="col-7">
                            <input type="email" class="form-control" ng-model="user.email">
                            <p class="text-danger" ng-repeat="error in errors.email">@{{ error }}</p>
                        </div>
                        <div class="col-5">
                            <label class="control-label">Phone</label>
                        </div>
                        <div class="col-7">
                            <input type="tel" class="form-control" ng-model="user.phone">
                            <p class="text-danger" ng-repeat="error in errors.phone">@{{ error }}</p>
                        </div>
                        <div class="col-5">
                            <label class="control-label">Birthday</label>
                        </div>
                        <div class="col-7">
                            <input type="date" class="form-control" ng-model="user.birthday">
                            <p class="text-danger" ng-repeat="error in errors.birthday">@{{ error }}</p>
                        </div>
                        <div class="col-5">
                            <label class="control-label">Password</label>
                        </div>
                        <div class="col-7">
                            <input type="password" class="form-control" ng-value="user.password" ng-model="user.password">
                            <p class="text-danger" ng-repeat="error in errors.password">@{{ error }}</p>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-block btn-primary" ng-click="register()">
                                <i class="fa fa-btn fa-sign-in"></i>Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>