<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">Events</div>
    </div>
    <table class="table">
        <tr ng-repeat="event in events">
            <td>@{{ event.name }}</td>
            <td class="text-nowrap">@{{ event.day }}</td>
            <td ng-if="!isAuthenticated()">
                <button class="btn btn-success" type="button" ng-click="subscription(event)">subscription</button>
            </td>
            <td ng-if="isAuthenticated() && !event.is_subscription">
                <button class="btn btn-success" type="button" ng-click="subscription(event)">subscription</button>
            </td>
            <td ng-if="isAuthenticated() && event.is_subscription">
                <button class="btn btn-danger" type="button" ng-click="unsubscribe(event)">unsubscribe</button>
            </td>
        </tr>
    </table>
</div>