<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">Users</div>
    </div>
    <table class="table">
        <tr ng-repeat="user in users">
            <td>@{{ user.name }}</td>
            <td>@{{ user.email }}</td>
            <td>@{{ user.phone }}</td>
            <td>@{{ user.birthday }}</td>
        </tr>
    </table>
</div>