<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Laravel
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a ui-sref="app.event.index">Events</a></li>
                <li ng-if="isAuthenticated()"><a ui-sref="app.user.index">Users</a></li>
            </ul>
            <!-- Right Side Of Navbar -->

            <ul class="nav navbar-nav navbar-right" ng-if="!isAuthenticated()">
                <li><a ui-sref="app.auth">Login</a></li>
                <li><a ui-sref="app.register">Register</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right" ng-if="isAuthenticated()">
                <li class="dropdown"><a href="#" ng-click="logout()">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container @{{ $state.current.name }} clearfix" ui-view="container"></div>