<div class="grid-center">
    <div class="col-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Auth</div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group" ng-if="error">
                        <p class="text-center text-danger">Ошибка авторизации проверьте введенные вами данные</p>
                    </div>
                    <div class="grid-center">
                        <div class="col-5">
                            <label for="email" class="control-label">E-Mail Address</label>
                        </div>
                        <div class="col-7">
                            <input id="email" type="email" class="form-control" ng-model="user.email">
                            <p class="text-danger" ng-repeat="error in errors.email">@{{ error }}</p>
                        </div>
                        <div class="col-5">
                            <label for="password" class="control-label">Password</label>
                        </div>
                        <div class="col-7">
                            <input id="password" type="password" class="form-control" ng-model="user.password">
                            <p class="text-danger" ng-repeat="error in errors.password">@{{ error }}</p>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-block btn-primary" ng-click="auth()">
                                <i class="fa fa-btn fa-sign-in"></i>Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>