<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body id="app-layout" ng-controller="AppCtrl">
<div ui-view></div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
