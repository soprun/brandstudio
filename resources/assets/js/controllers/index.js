import angular from "angular";

import {AppCtrl} from "./AppCtrl";
import {AuthCtrl} from "./AuthCtrl";
import {RegisterCtrl} from "./RegisterCtrl";
import {EventCtrl} from "./EventCtrl";
import {UserCtrl} from "./UserCtrl";

export default function () {
    'use strict';

    let app = angular.module('App');

    app.controller('AppCtrl', AppCtrl);
    app.controller('AuthCtrl', AuthCtrl);
    app.controller('RegisterCtrl', RegisterCtrl);

    app.controller('EventCtrl', EventCtrl);
    app.controller('UserCtrl', UserCtrl);
}
