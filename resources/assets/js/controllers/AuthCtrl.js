class AuthCtrl {
    constructor($scope, $state, $auth) {
        $scope.user = {
            email: 'demo@demo.com',
            password: 'demo@demo.com'
        };
        $scope.errors = null;
        $scope.error = false;

        this.init($scope, $state, $auth);
    }

    init($scope, $state, $auth) {
        $scope.auth = function () {
            $auth.login($scope.user)
                .then(function (response) {
                    $scope.error = false;

                    $state.go('app.home');
                })
                .catch(function (response) {
                    $scope.errors = response.data.errors;

                    if (response.status == 401) {
                        $scope.error = true;
                    }
                });
        };
    }
}

AuthCtrl.$inject = ['$scope', '$state', '$auth'];

export {AuthCtrl}
