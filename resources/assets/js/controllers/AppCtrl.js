class AppCtrl {
    constructor($scope, $auth) {
        this.init($scope, $auth);
    }

    init($scope, $auth) {
        $scope.isAuthenticated = function () {
            return $auth.isAuthenticated();
        };
        $scope.logout = function () {
            return $auth.logout();
        }
    }
}

AppCtrl.$inject = ['$scope', '$auth'];

export {AppCtrl}
