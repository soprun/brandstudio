class RegisterCtrl {
    constructor($scope, $state, $auth) {
        let birthday = new Date();
        birthday.toLocaleDateString();

        $scope.user = {
            name: 'demo',
            email: 'demo@demo.com',
            phone: '+7 888 888-88-88',
            birthday: birthday,
            password: 'demo@demo.com'
        };
        $scope.errors = null;

        this.init($scope, $state, $auth);
    }

    init($scope, $state, $auth) {
        $scope.register = function () {
            $auth.signup($scope.user)
                .then(function (response) {
                    $auth.setToken(response.data.token);
                    $state.go('app.home');
                })
                .catch(function (response) {
                    $scope.errors = response.data.errors;
                });
        };
    }
}

RegisterCtrl.$inject = ['$scope', '$state', '$auth'];

export {RegisterCtrl}
