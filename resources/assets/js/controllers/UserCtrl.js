class UserCtrl {
    constructor($scope, $http) {
        $scope.users = null;

        this.init($scope, $http);
    }

    init($scope, $http) {

        function init() {
            $http
                .get('/api/user')
                .success(function (response) {
                    $scope.users = response.data;
                });
        }

        init();
    }
}

UserCtrl.$inject = ['$scope', '$http'];

export {UserCtrl}
