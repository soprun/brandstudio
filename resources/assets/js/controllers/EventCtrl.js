class EventCtrl {
    constructor($auth, $scope, $state, $http) {
        $scope.events = null;

        this.init($auth, $scope, $state, $http);
    }

    init($auth, $scope, $state, $http) {

        function init() {
            $http
                .get('/api/event')
                .success(function (response) {
                    $scope.events = response.data;
                });
        }

        init();

        $scope.subscription = function (event) {
            if ($auth.isAuthenticated() == false) {
                $state.go('app.auth');
            }

            $http.get('/api/event/' + event.id + '/subscription').success(function () {
                init();
            })
        };
        $scope.unsubscribe = function (event) {
            if ($auth.isAuthenticated() == false) {
                $state.go('app.auth');
            }

            $http.get('/api/event/' + event.id + '/unsubscribe').success(function () {
                init();
            })
        };
    }
}

EventCtrl.$inject = ['$auth', '$scope', '$state', '$http'];

export {EventCtrl}
