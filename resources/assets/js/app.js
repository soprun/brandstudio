'use strict';

import angular from "angular";
import "angular-ui-router";
import "satellizer";
import controllers from "./controllers/index";
// import filters from './filters/index';
// import services from './services/index';
// import factory from './factory/index'
// import directives from './directives/index';

angular.module('App', [
    'ui.router',
    'satellizer'
]);

angular.module('App')
    .config([
        '$stateProvider',
        '$locationProvider',
        '$urlRouterProvider',
        '$authProvider',
        function ($stateProvider, $locationProvider, $urlRouterProvider, $authProvider) {

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $authProvider.loginUrl = '/api/auth/login';
            $authProvider.signupUrl = '/api/auth/signup';

            // $urlRouterProvider.otherwise('/auth');

            $stateProvider
                .state('app', {
                    abstract: true,
                    templateUrl: '/template/common.content'
                })
                .state('app.home', {
                    url: '/',
                    views: {
                        'container@app': {
                            templateUrl: '/template/event.row',
                            controller: 'EventCtrl'
                        }
                    }
                })
                .state('app.auth', {
                    url: '/auth',
                    views: {
                        'container@app': {
                            templateUrl: '/template/auth',
                            controller: 'AuthCtrl'
                        }
                    }
                })
                .state('app.register', {
                    url: '/register',
                    views: {
                        'container@app': {
                            templateUrl: '/template/register',
                            controller: 'RegisterCtrl'
                        }
                    }
                })
                .state('app.event', {
                    abstract: true,
                    url: '/event'
                })
                .state('app.event.index', {
                    url: '',
                    views: {
                        'container@app': {
                            templateUrl: '/template/event.row',
                            controller: 'EventCtrl'
                        }
                    }
                })
                .state('app.user', {
                    abstract: true,
                    url: '/user'
                })
                .state('app.user.index', {
                    url: '',
                    views: {
                        'container@app': {
                            templateUrl: '/template/user.row',
                            controller: 'UserCtrl'
                        }
                    }
                })
        }])
    .run(['$rootScope', '$state', '$auth', function ($rootScope, $state, $auth) {
        $rootScope.$state = $state;

        // $rootScope
        //     .$on('$stateChangeStart', function (event, toState) {
        //         if ($auth.isAuthenticated()) {
        //
        //         }
        //         console.log($auth.isAuthenticated());
        //     });
    }]);

// filters();
// factory();
// services();
// directives();
controllers();
