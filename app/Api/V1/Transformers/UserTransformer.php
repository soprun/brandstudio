<?php

namespace App\Api\V1\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'events',
    ];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'       => $user->id,
            'name'     => $user->name,
            'email'    => $user->email,
            'phone'    => $user->phone,
            'birthday' => $user->birthday !== null ? $user->birthday->toDateTimeString() : null,
        ];
    }

    /**
     * Выводит список событий пользователя
     *
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeEvents(User $user)
    {
        return $this->collection($user->events, new EventTransformer());
    }
}
