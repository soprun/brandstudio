<?php

namespace App\Api\V1\Transformers;

use App\Models\Event;
use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users',
    ];

    /**
     * @param Event $event
     * @return array
     */
    public function transform(Event $event)
    {
        $is_subscription = false;

        $user = app('Dingo\Api\Auth\Auth')->user();

        if ($user !== null) {
            $is_subscription = $event->users->where('id', $user->id)->first() ? true : false;
        }

        return [
            'id'              => $event->id,
            'name'            => $event->name,
            'day'             => $event->day !== null ? $event->day->toDateTimeString() : null,
            'created_at'      => $event->created_at->toDateTimeString(),
            'updated_at'      => $event->updated_at->toDateTimeString(),
            'is_subscription' => $is_subscription,
        ];
    }

    /**
     * Выводит список участников события
     *
     * @param Event $event
     * @return \League\Fractal\Resource\Item
     */
    public function includeUsers(Event $event)
    {
        return $this->collection($event->users, new UserTransformer());
    }
}
