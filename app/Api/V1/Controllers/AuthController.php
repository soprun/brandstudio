<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\AuthRequest;
use App\Api\V1\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    use Helpers;

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function signup(UserRequest $request)
    {
        $data = $request->input();

        $user = User::create($data);

        if ($user->id === null) {
            return $this->response->error('could_not_create_user', 500);
        }

        return $this->login($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
            }
        } catch (JWTException $e) {
            return $this->response->error('could_not_create_token', 500);
        }

        return response()->json(compact('token'));
    }
}