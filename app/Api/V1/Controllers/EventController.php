<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\EventRequest;
use App\Api\V1\Transformers\EventTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Event;
use Dingo\Api\Routing\Helpers;

class EventController extends Controller
{
    use Helpers;

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['index', 'show']]);
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        $events = Event::latest()->with(['users'])->paginate();

        return $this->response->paginator($events, new EventTransformer);
    }

    /**
     * @param Event $event
     * @return \Dingo\Api\Http\Response
     */
    public function show(Event $event)
    {
        return $this->response->item($event, new EventTransformer());
    }

    /**
     * @param Event $event
     * @return \Dingo\Api\Http\Response
     */
    public function subscription(Event $event)
    {
        $user = $this->auth()->user();

        if (!$user->events()->find($event->id)) {
            $user->events()->attach($event);

            return $this->response->created();
        }

        return $this->response->noContent();
    }

    /**
     * @param Event $event
     * @return \Dingo\Api\Http\Response
     */
    public function unsubscribe(Event $event)
    {
        $user = $this->auth()->user();

        if ($user->events()->find($event->id)) {
            $user->events()->detach($event);

            return $this->response->created();
        }

        return $this->response->noContent();
    }

    public function store(EventRequest $request)
    {
        $data = $request->input();

        if ($event = Event::create($data)) {
            return $this->response->item($event, new EventTransformer());
        }

        return $this->response->errorBadRequest('create error');
    }

    public function destroy(Event $event)
    {
        $event->delete();

        return $this->response->noContent();
    }
}
