<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\UserRequest;
use App\Api\V1\Transformers\EventTransformer;
use App\Api\V1\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use Dingo\Api\Routing\Helpers;

class UserController extends Controller
{
    use Helpers;

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $users = User::latest()->paginate();

        return $this->response->paginator($users, new UserTransformer());
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return $this->response->item($user, new UserTransformer());
    }

    public function events()
    {
        $user = $this->auth()->user();

        return $this->response->collection($user->events, new EventTransformer());
    }

    public function store(UserRequest $request)
    {
        $data = $request->input();

        if ($user = User::create($data)) {
            return $this->response->item($user, new  UserTransformer());
        }

        return $this->response->errorBadRequest('create error');
    }

    public function destroy($id) {

        User::findOrFail($id)->delete();

        return $this->response->noContent();
    }
}
