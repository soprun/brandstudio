<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class AuthRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'    => 'required|email|max:255',
            'password' => 'required|min:6',
        ];
    }
}