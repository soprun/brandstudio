<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:user',
            'phone'    => 'max:255|unique:user', // required|unique:user
            'birthday' => 'date',
            'password' => 'required|min:6',
        ];
    }
}