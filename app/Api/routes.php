<?php

/** @var Dingo\Api\Routing\Router $router */

$router->version('v1', [
    'namespace'  => 'App\Api\V1\Controllers',
    'middleware' => ['api.throttle'],
], function ($router) {
    /** @var Dingo\Api\Routing\Router $router */

    $router->group([
        'middleware' => ['api.auth'],
    ], function ($router) {
        /** @var Dingo\Api\Routing\Router $router */

        $router->get('user/events', 'UserController@events');
        $router->get('event/{event}/subscription', 'EventController@subscription');
        $router->get('event/{event}/unsubscribe', 'EventController@unsubscribe');
    });

    $router->post('auth/login', 'AuthController@login');
    $router->post('auth/signup', 'AuthController@signup');

    $router->resource('event', 'EventController');
    $router->resource('user', 'UserController');
});
