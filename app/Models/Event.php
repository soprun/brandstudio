<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Event
 *
 * @property integer                                                          $id
 * @property string                                                           $name
 * @property \Carbon\Carbon                                                   $day
 * @property \Carbon\Carbon                                                   $created_at
 * @property \Carbon\Carbon                                                   $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereDay($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed                                                       $is_subscription
 */
class Event extends Model
{
    protected $table = 'event';

    protected $fillable = [
        'name',
        'day',
    ];

    protected $dates = [
        'day',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'event_subscription', 'event_id', 'user_id');
    }

    public function getIsSubscriptionAttribute() : bool
    {
        if (auth()->check() === false) {
            return false;
        }

        return $this->users->where('id', auth()->user()->id)->first() ? true : false;
    }
}
