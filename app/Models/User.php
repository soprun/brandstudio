<?php

namespace App\Models;

use App\Traits\FillableTrait;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\User
 *
 * @property integer                                                           $id
 * @property string                                                            $name
 * @property string                                                            $email
 * @property string                                                            $phone
 * @property \Carbon\Carbon                                                    $birthday
 * @property string                                                            $password
 * @property string                                                            $remember_token
 * @property \Carbon\Carbon                                                    $created_at
 * @property \Carbon\Carbon                                                    $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use FillableTrait;

    protected $table = 'user';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'birthday',
        'password',
    ];

    protected $dates = [
        'birthday',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    public function events()
    {
        return $this->belongsToMany(User::class, 'event_subscription', 'user_id', 'event_id');
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = $this->nullFillable($phone);
    }

    public function setBirthdayAttribute($date)
    {
        if ($date instanceof \DateTime) {
            return $this->attributes['birthday'] = $date;
        }
        
        return $this->attributes['birthday'] = new Carbon($date);
    }
}
