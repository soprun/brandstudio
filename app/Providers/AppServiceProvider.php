<?php

namespace App\Providers;

use Barryvdh\Debugbar\Facade as Debugbar;
use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);

            if (!$this->app->runningInConsole() && !$this->app->runningUnitTests()) {
                $this->app->register(DebugbarServiceProvider::class);
                $this->app->alias('Debugbar', Debugbar::class);
            }

            if ($this->app->make('request')->is('api/*')) {
                $this->app->make('debugbar')->disable();
            }
            $this->app->singleton(FakerGenerator::class, function () {
                return FakerFactory::create('ru_RU');
            });
        }
    }
}
