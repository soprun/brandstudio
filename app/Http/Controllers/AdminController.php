<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Event;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $events = (new Event)
            ->orderBy('day')
            ->with(['users'])
            ->paginate();

        return view('admin.dashboard', compact('events'));
    }
}
