<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class PageController extends Controller
{
    public function layout()
    {
        return view('layouts.app');
    }
}
