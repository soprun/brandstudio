<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Event;

class EventController extends Controller
{
    private $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function show($id)
    {
        $event = $this->event->with(['users'])->findOrFail($id);

        return view('admin.events.show', compact('event'));
    }
}
