<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    private $users;

    public function __construct(User $user)
    {
        $this->users = $user;
    }

    public function index()
    {
        $users = $this->users->latest()->paginate();

        return view('admin.users.index', compact('users'));
    }

    public function show($id)
    {
        $user = $this->users->findOrFail($id);

        return view('admin.users.show', compact('user'));
    }
}
