<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('template/{name}', [
    'as' => 'template',
    function ($name) {
        $view = 'template.' . $name;

        if (view()->exists($view)) {
            if (app()->isLocal()) {
                app('debugbar')->disable();
            }

            return view()->make($view);
        }

        abort(404);

        return null;
    },
]);

Route::get('/', [
    'as'   => 'home',
    'uses' => 'PageController@layout',
]);

Route::get('{slug}', [
    'as'   => 'slug',
    'uses' => 'PageController@layout',
]);