<?php

namespace App\Traits;

trait FillableTrait
{
    /**
     * @param string|null $input
     * @return null|string
     */
    public function nullFillable($input)
    {
        if ($input === '') {
            return null;
        }

        return trim((string)$input) === '' ? null : trim($input);
    }

    /**
     * @param string|null $input
     * @return string|null
     */
    public function contentFillable($input)
    {
        if ($input === '') {
            return null;
        }

        $search = [
            '“',
            '”',
        ];
        $replace = [
            '"',
            '"',
        ];

        return str_replace($search, $replace, $input);
    }
}
