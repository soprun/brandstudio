<?php

use Illuminate\Database\Seeder;
use App\Models\{
    Event, User
};

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, random_int(100, 150))->create()->each(function (Event $event) {
            $random = random_int(0, 20);

            for ($i = 0; $i < $random; $i++) {
                /** @var User $random_user */
                $random_user = User::orderBy(DB::raw('RAND()'))->take(1)->get();

                $event->users()->attach($random_user);
            }
        });
    }
}
