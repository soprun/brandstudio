<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, random_int(20, 40))->create()->each(function (User $user) {
            // ..
        });

        factory(User::class)->create([
            'name' => 'demo',
            'email' => 'demo@demo.com',
            'phone' => '+7 888 888-88-88',
            'birthday' => '2016-05-21',
            'password' => 'demo@demo.com',
        ]);
    }
}
